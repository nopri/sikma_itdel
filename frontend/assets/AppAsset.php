<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $css = [
        'css/bootstrap.min.css',
        'css/fancybox/jquery.fancybox.css',
        'css/jcarousel.css', 
        'css/flexslider.css',
        'css/style.css',
        // 'css/site.css',
    ];
    public $js = [
        '//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js',
        'js/jquery.js',
        'js/jquery.easing.1.3.js',
        'js/bootstrap.min.js',
        'js/jquery.fancybox.pack.js',
        'js/jquery.fancybox-media.js',
        'js/google-code-prettify/prettify.js',
        'js/portfolio/jquery.quicksand.js',
        'js/portfolio/setting.js',
        'js/jquery.flexslider.js',
        'js/animate.js',
        'js/custom.js',
    ];

}