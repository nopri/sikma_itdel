<?php

namespace frontend\controllers;

use Yii;
use app\models\Kompetisi;
use app\models\KompetisiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * KompetisiController implements the CRUD actions for Kompetisi model.
 */
class KompetisiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['GET'],
                ],
            ],
        ];
    }

    /**
     * Lists all Kegiatan models.
     * @return mixed
     */
    public function actionIndex()
    {


        $query = Kompetisi::find();
        $searchModel = new KompetisiSearch();
        $searchModel->id_mhs = Yii::$app->user->id;
        
        $kompetisi = $query->all();
        $queryParams = Yii::$app->request->queryParams;
        if ( isset($queryParams['Kompetisi']['id_mhs']) ) {
            $queryParams['Kompetisi']['id_mhs'] = Yii::$app->user->id;
        }
        $dataProvider = $searchModel->search($queryParams);

        if (Yii::$app->user->isGuest) {
            $this->redirect(Yii::$app->urlManager->createUrl(['site/login']));
        }else{
          return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'kompetisi' => $kompetisi,
        ]);  
      }
  }

    /**
     * Displays a single Kegiatan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionPdf($id) {
    $model = Kompetisi::findOne($id);
        
    

    // This will need to be the path relative to the root of your app.
    $filePath = '/web/Kompetisi Mahasiswa/';
    // Might need to change '@app' for another alias
    $completePath = Yii::getAlias('@frontend'.$filePath.'/'.$model->upload_proposal);

    
    return Yii::$app->response->sendFile($completePath, $filePath, ['inline'=>true]);

        
}

    /**
     * Creates a new Kegiatan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Kompetisi();
        if ($model->load(Yii::$app->request->post())){
          $model->id_mhs = Yii::$app->user->id;
          $model->upload_proposal = "Initial";
          $model->save();
          $id_kompetisi = $model->id_kompetisi;
          $file = UploadedFile::getInstance($model, 'upload_proposal');
          $fileName = $file->baseName . '.' . $file->getExtension();
          $file->saveAs(Yii::getAlias('@frontend') . '/web/Kompetisi Mahasiswa/' . $fileName);
          $model->upload_proposal = $fileName;
          $model->save();
          
          return $this->redirect(['index', 'id' => $model->id_kompetisi]);
      }else{
        return $this->render('create', [
            'model' => $model,
        ]);
    }
}

    /**
     * Updates an existing Kegiatan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())){
            $model->id_mhs = Yii::$app->user->id;
            $id_kompetisi = $model->id_kompetisi;
            $file = UploadedFile::getInstance($model, 'upload_proposal');
            $fileName = 'kompetisi' . $id_kompetisi . '.' .$file->getExtension();
            $file->saveAs(Yii::getAlias('@frontend') . '/web/Kompetisi Mahasiswa/' . $fileName);
            $model->upload_proposal = $fileName;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id_kompetisi]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionPdf($id) {
        $model = Kompetisi::findOne($id);

    // This will need to be the path relative to the root of your app.
        $filePath = '/web/Kompetisi Mahasiswa/';
    // Might need to change '@app' for another alias
        $completePath = Yii::getAlias('@frontend'.$filePath.'/'.$model->upload_proposal);


        return Yii::$app->response->sendFile($completePath, $filePath, ['inline'=>true]);


    }


    /**
     * Deletes an existing Kegiatan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDownload($id) 
    { 
        $download = Kompetisi::findOne($id); 
        $path=Yii::getAlias('@frontend') . '/web/Kompetisi Mahasiswa/' . $download->upload_proposal;

        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        }
    }


    /**
     * Finds the Kegiatan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kegiatan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kompetisi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function actionViewFile($fileName){
        $file = Yii::getAlias('@frontend' . '/web/Kompetisi Mahasiswa/' . $fileName);
        return Yii::$app->response->sendFile($file, NULL, ['inline' => TRUE]);
    }
}
