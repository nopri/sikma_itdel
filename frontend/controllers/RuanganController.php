<?php

namespace frontend\controllers;

use Yii;
use app\models\Ruangan;
use app\models\RuanganSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use frontend\models\LoginForm;
use yii\helpers\Url;
use app\models\UploadForm;

/**
 * RuanganController implements the CRUD actions for Ruangan model.
 */
class RuanganController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ruangan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = Ruangan::find();
        $searchModel = new RuanganSearch();
        $searchModel->id_mhs = Yii::$app->user->id;
        
        $ruangan = $query->all();
        $queryParams = Yii::$app->request->queryParams;
        if ( isset($queryParams['Ruangan']['id_mhs']) ) {
            $queryParams['Ruangan']['id_mhs'] = Yii::$app->user->id;
        }
        $dataProvider = $searchModel->search($queryParams);

        if (Yii::$app->user->isGuest) {
            $this->redirect(Yii::$app->urlManager->createUrl(['site/login']));
        }else{
          return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ruangan' => $ruangan,
        ]);  
      }

      
  }

    /**
     * Displays a single Ruangan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ruangan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        
        $model = new Ruangan();

        if ($model->load(Yii::$app->request->post())) {
            $model->id_mhs = Yii::$app->user->id;
            $id_ruangan = $model->id_ruangan;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id_ruangan]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ruangan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_ruangan]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ruangan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ruangan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ruangan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ruangan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
