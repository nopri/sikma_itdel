<?php

namespace frontend\controllers;

use Yii;
use app\models\LpjKompetisi;
use app\models\LpjKompetisiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\UploadForm;

/**
 * LpjKompetisiController implements the CRUD actions for LpjKompetisi model.
 */
class LpjKompetisiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LpjKompetisi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = LpjKompetisi::find();
        $searchModel = new LpjKompetisiSearch();
        $searchModel->id_mhs = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $lpj_kompetisi = $query->all();
        $queryParams = Yii::$app->request->queryParams;
        if ( isset($queryParams['LpjKompetisi']['id_mhs']) ) {
            $queryParams['LpjKompetisi']['id_mhs'] = Yii::$app->user->id;
        }
        $dataProvider = $searchModel->search($queryParams);

        if (Yii::$app->user->isGuest) {
            $this->redirect(Yii::$app->urlManager->createUrl(['site/login']));
        }else{
          return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'lpj_kompetisi' => $lpj_kompetisi,
        ]);  
      }
  }
    /**
     * Displays a single LpjKompetisi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionPdf($id) {
    $model = LpjKompetisi::findOne($id);
        
    

    // This will need to be the path relative to the root of your app.
    $filePath = '/web/LPJ Kompetisi/';
    // Might need to change '@app' for another alias
    $completePath = Yii::getAlias('@frontend'.$filePath.'/'.$model->file_lpj);

    
    return Yii::$app->response->sendFile($completePath, $filePath, ['inline'=>true]);

        
}

    /**
     * Creates a new LpjKompetisi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {$model = new LpjKompetisi();
        if ($model->load(Yii::$app->request->post())) {
            $model->id_mhs = Yii::$app->user->id;
            $model->file_lpj = "Initial";
            $model->save();

            $id_LpjKompetisi = $model->id_LpjKompetisi;
            $file = UploadedFile::getInstance($model, 'file_lpj');
            $fileName = $file->baseName . '.' . $file->getExtension();
            $file->saveAs(Yii::getAlias('@frontend') . '/web/LPJ Kompetisi/' . $fileName);
            $model
            ->file_lpj = $fileName;
            $model->save();

            return $this->redirect(['index', 'id' => $model->id_LpjKompetisi]);
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

     //Action Download
    public function actionDownload($id) 
    { 
        $download = LpjKompetisi::findOne($id); 
        $path=Yii::getAlias('@frontend') . '/web/LPJ Kompetisi/' . $download->file_lpj;

        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        }
    }

    /**
     * Updates an existing LpjKompetisi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $id_LpjKompetisi = $model->id_LpjKompetisi;
            $file = UploadedFile::getInstance($model, 'file_lpj');
            $fileName = $file->baseName . '.' .$file->getExtension();
            $file->saveAs(Yii::getAlias('@frontend') . '/web/LPJ Kompetisi/' . $fileName);
            $model->file_lpj = $fileName;
            $model->save();
            return $this->redirect(['view', 'id_LpjKompetisi' => $model->id_LpjKompetisi]);
        }else{
            return $this->render('update', [
                'model' => $model,
            ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    /**
     * Deletes an existing LpjKompetisi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionPdf($id) {
        $model = LpjKompetisi::findOne($id);

    // This will need to be the path relative to the root of your app.
        $filePath = '/web/LPJ Kompetisi/';
    // Might need to change '@app' for another alias
        $completePath = Yii::getAlias('@frontend'.$filePath.'/'.$model->file_lpj);


        return Yii::$app->response->sendFile($completePath, $filePath, ['inline'=>true]);


    }
    /**
     * Finds the LpjKompetisi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LpjKompetisi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LpjKompetisi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function actionViewFile($fileName){
        $file = Yii::getAlias('@frontend' . '/web/LPJ Kompetisi' . $fileName);
        return Yii::$app->response->sendFile($file, NULL, ['inline' => TRUE]);
    }
}


