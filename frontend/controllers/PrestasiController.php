<?php

namespace frontend\controllers;

use Yii;
use app\models\Prestasi;
use app\models\PrestasiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\UploadForm;
use app\models\NewUser;

/**
 * PrestasiController implements the CRUD actions for Prestasi model.
 */
class PrestasiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Prestasi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = Prestasi::find();
        $searchModel = new PrestasiSearch();
        $searchModel->id_mhs = Yii::$app->user->id;
        
        $prestasi_ = $query->all();
        $queryParams = Yii::$app->request->queryParams;
        if ( isset($queryParams['Prestasi']['id_mhs']) ) {
            $queryParams['Prestasi']['id_mhs'] = Yii::$app->user->id;
        }
        $dataProvider = $searchModel->search($queryParams);

        if (Yii::$app->user->isGuest) {
            $this->redirect(Yii::$app->urlManager->createUrl(['site/login']));
        }else{
          return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'prestasi_' => $prestasi_,
        ]);  
      }

  }

    /**
     * Displays a single Prestasi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionPdf($id) {
    $model = Prestasi::findOne($id);
        
    

    // This will need to be the path relative to the root of your app.
    $filePath = '/web/Prestasi Mahasiswa/';
    // Might need to change '@app' for another alias
    $completePath = Yii::getAlias('@frontend'.$filePath.'/'.$model->upload_file);

    
    return Yii::$app->response->sendFile($completePath, $filePath, ['inline'=>true]);

        
}

    public function actionView_Tolak($id)
    {
        return $this->render('view_tolak', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionView_Menunggu($id)
    {
        return $this->render('view_menunggu', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUpdate1($id)
    {
        return $this->render('update', [
            'model' => $this->findModel($id),
        ]);
    }



    /**
     * Creates a new Prestasi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Prestasi();
        if ($model->load(Yii::$app->request->post())) {
            $model->id_mhs = Yii::$app->user->id;
            $model->upload_file = "Initial";
            $model->save();
            $id_prestasi = $model->id_prestasi;
            $file = UploadedFile::getInstance($model, 'upload_file');
            $fileName = $file->baseName . '.' . $file->getExtension();
            $file->saveAs(Yii::getAlias('@frontend') . '/web/Prestasi Mahasiswa/' . $fileName);
            $model->upload_file = $fileName;
            $model->save();
            
            return $this->redirect(['index', 'id' => $model->id_prestasi]);
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Prestasi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $id_prestasi = $model->id_prestasi;
            $file = UploadedFile::getInstance($model, 'upload_file');
            $fileName = 'prestasi_' . $id_prestasi . '.' .$file->getExtension();
            $file->saveAs(Yii::getAlias('@frontend') . '/web/Prestasi Mahasiswa/' . $fileName);
            $model->upload_file = $fileName;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id_prestasi]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Prestasi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        
        return $this->redirect(['index']);
    }

    public function actionDownload($id) 
    { 
      $download = Prestasi::findOne($id); 
      $path=Yii::getAlias('@frontend') . '/web/Prestasi Mahasiswa/' . $download->upload_file;
      
      if (file_exists($path)) {
          return Yii::$app->response->sendFile($path);
      }
  }


  public function actionPdf($id) {
    $model = Prestasi::findOne($id);

    // This will need to be the path relative to the root of your app.
    $filePath = '/web/Prestasi Mahasiswa/';
    // Might need to change '@app' for another alias
    $completePath = Yii::getAlias('@frontend'.$filePath.'/'.$model->upload_file);

    
    return Yii::$app->response->sendFile($completePath, $filePath, ['inline'=>true]);


}

    /**
     * Finds the Prestasi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Prestasi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Prestasi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function actionViewFile($fileName){
        $file = Yii::getAlias('@frontend' . '/web/Prestasi Mahasiswa/' . $fileName);
        return Yii::$app->response->sendFile($file, NULL, ['inline' => TRUE]);
    }
}
