<?php

namespace frontend\controllers;

use Yii;
use app\models\LPJKegiatan;
use app\models\LPJKegiatanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\UploadForm;
use frontend\models\NewUser;

/**
 * LPJKegiatanController implements the CRUD actions for LPJKegiatan model.
 */
class LPJKegiatanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LPJKegiatan models.
     * @return mixed
     */
    public function actionIndex()
    {

        $query = LPJKegiatan::find();
        $searchModel = new LPJKegiatanSearch();
        $searchModel->id_mhs = Yii::$app->user->id;
        
        $lpj_kompetisi = $query->all();
        $queryParams = Yii::$app->request->queryParams;
        if ( isset($queryParams['LPJKegiatan']['id_mhs']) ) {
            $queryParams['LPJKegiatan']['id_mhs'] = Yii::$app->user->id;
        }
        $dataProvider = $searchModel->search($queryParams);

        if (Yii::$app->user->isGuest) {
            $this->redirect(Yii::$app->urlManager->createUrl(['site/login']));
        }else{
          return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'lpj_kompetisi' => $lpj_kompetisi,
        ]);  
      }
  }

    /**
     * Displays a single LPJKegiatan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionPdf($id) {
    $model = LPJKegiatan::findOne($id);
        
    

    // This will need to be the path relative to the root of your app.
    $filePath = '/web/LPJ Kegiatan/';
    // Might need to change '@app' for another alias
    $completePath = Yii::getAlias('@frontend'.$filePath.'/'.$model->file_lpj);

    
    return Yii::$app->response->sendFile($completePath, $filePath, ['inline'=>true]);

        
}
    /**
     * Creates a new LPJKegiatan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new LpjKegiatan();
        if ($model->load(Yii::$app->request->post())) {
            $model->id_mhs = Yii::$app->user->id;
            $model->file_lpj = "Initial";
            $model->save();

            $id_LpjKegiatan = $model->id_LpjKegiatan;
            $file = UploadedFile::getInstance($model, 'file_lpj');
            $fileName = $file->baseName . '.' . $file->getExtension();
            $file->saveAs(Yii::getAlias('@frontend') . '/web/LPJ Kegiatan/' . $fileName);
            $model
            ->file_lpj = $fileName;
            $model->save();

            return $this->redirect(['index', 'id' => $model->id_LpjKegiatan]);
        }else{
            return $this->render('create', [
                'model' => $model,

            ]);
        }
    }

     //Action Download
    public function actionDownload($id) 
    { 
        $download = LpjKegiatan::findOne($id); 
        $path=Yii::getAlias('@frontend') . '/web/LPJ Kegiatan/' . $download->file_lpj;

        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        }
    }

    /**
     * Updates an existing LPJKegiatan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $id_LpjKegiatan = $model->id_LpjKegiatan;
            $file = UploadedFile::getInstance($model, 'file_lpj');
            $fileName = $file->baseName . '.' .$file->getExtension();
            $file->saveAs(Yii::getAlias('@frontend') . '/web/LPJ Kegiatan/' . $fileName);
            $model->file_lpj = $fileName;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id_LpjKegiatan]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionPdf($id) {
        $model = LPJKegiatan::findOne($id);

    // This will need to be the path relative to the root of your app.
        $filePath = '/web/LPJ Kegiatan/';
    // Might need to change '@app' for another alias
        $completePath = Yii::getAlias('@frontend'.$filePath.'/'.$model->file_lpj);


        return Yii::$app->response->sendFile($completePath, $filePath, ['inline'=>true]);


    }

    /**
     * Deletes an existing LPJKegiatan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LPJKegiatan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LPJKegiatan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LpjKegiatan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function actionViewFile($fileName){
        $file = Yii::getAlias('@frontend' . '/web/LPJ Kegiatan' . $fileName);
        return Yii::$app->response->sendFile($file, NULL, ['inline' => TRUE]);
    }
}
