<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Kompetisi */

$this->title = 'Tambah Kompetisi';
$this->params['breadcrumbs'][] = ['label' => 'Kompetisi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kompetisi-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
