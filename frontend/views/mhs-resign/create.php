<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\MhsResign */

$this->title = Yii::t('app', 'Create Mhs Resign');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mhs Resigns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mhs-resign-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
