<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\MhsResign */

$this->title = Yii::t('app', 'Update Mhs Resign: {name}', [
    'name' => $model->id_resign,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mhs Resigns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_resign, 'url' => ['view', 'id' => $model->id_resign]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="mhs-resign-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
