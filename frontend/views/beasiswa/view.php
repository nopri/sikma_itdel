<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Beasiswa */

$this->title = 'Data Beasiswa';
$this->params['breadcrumbs'][] = ['label' => 'Beasiswa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="beasiswa-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id_beasiswa',
            // 'id_mhs',
            'nama_mhs',
            'nim_mhs',
            'alamat',
            'jlh_bersaudara',
            'anak_ke',
            'tanggungan',
            'pekerjaan_ayah',
            'pekerjaan_ibu',
            'penghasilan_ayah',
            'penghasilan_ibu',
            ['label' => 'Status Request', 'value' => $model->statusRequest->name],
        ],
    ]) ?>

</div>
