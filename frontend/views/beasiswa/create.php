<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Beasiswa */

$this->title = 'Request Beasiswa';
$this->params['breadcrumbs'][] = ['label' => 'Beasiswa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beasiswa-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
