<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\BeasiswaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="beasiswa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_beasiswa') ?>

    <?= $form->field($model, 'id_mhs') ?>

    <?= $form->field($model, 'nama_mhs') ?>

    <?= $form->field($model, 'alamat') ?>

    <?= $form->field($model, 'nim_mhs') ?>

    <?php // echo $form->field($model, 'jlh_bersaudara') ?>

    <?php // echo $form->field($model, 'anak_ke') ?>

    <?php // echo $form->field($model, 'tanggungan') ?>

    <?php // echo $form->field($model, 'pekerjaan_ayah') ?>

    <?php // echo $form->field($model, 'pekerjaan_ibu') ?>

    <?php // echo $form->field($model, 'penghasilan_ayah') ?>

    <?php // echo $form->field($model, 'penghasilan_ibu') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
