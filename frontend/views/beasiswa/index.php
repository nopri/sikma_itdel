<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use common\components\ToolsColumn;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\BeasiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$asset = frontend\assets\AppAsset::register($this);
$baseUrl = $asset->baseUrl;

$this->title = 'Beasiswa';
$this->params['breadcrumbs'][] = $this->title;
$uiHelper=\Yii::$app->uiHelper;
?>

<div class="beasiswa-index">

  <?= $uiHelper->renderContentHeader($this->title);?>
  <?php

  $toolbarItemMenu =  
  "<a href='".Url::to(['create'])."' class='btn btn-success'><i class='fa fa-book'></i><span class='toolbar-label'>Request Beasiswa</span></a>
  "
  ;

  ?>


  <?=Yii::$app->uiHelper->renderToolbar([
    'pull-left' => true,
    'groupTemplate' => ['groupStatusExpired'],
    'groups' => [
      'groupStatusExpired' => [
        'template' => ['filterStatus'],
        'buttons' => [
          'filterStatus' => $toolbarItemMenu,
        ]
      ],
    ],
  ]) ?>


  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    // 'filterModel' => $searchModel,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],
      'nama_mhs',
      'nim_mhs',
      'alamat',
      // 'jlh_bersaudara',
      // 'anak_ke',
      // 'tanggungan',
      // 'pekerjaan_ayah',
      // 'pekerjaan_ibu',
      // 'penghasilan_ayah',
      // 'penghasilan_ibu',
      ['attribute' => 'status_request_id',
      'label' => 'Status Request',
      'value' => function($model){
        if(is_null($model->statusRequest['name'])){
          return '-';
        }else{
          return $model->statusRequest['name'];
        }
      }
    ],

    ['class' => 'common\components\ToolsColumn',
    'template' => '{view} {edit}',
    'header' => 'Aksi',
    'buttons' => [
      'view' => function ($url, $model){
        return ToolsColumn::renderCustomButton($url, $model, 'View Detail', 'fa fa-eye');
      },
      'edit' => function ($url, $model){
        if ($model->status_request_id == 1 || $model->status_request_id == 2) {
          return "";
        }else{
          return ToolsColumn::renderCustomButton($url, $model, 'Edit', 'fa fa-times');
        }
      },
    ],
    'urlCreator' => function ($action, $model, $key, $index){
      if ($action === 'view') {
        return Url::toRoute(['view', 'id' => $key]);
      }else if ($action === 'edit') {
        return Url::toRoute(['update', 'id' => $key]);
      }

    }
  ],
],

]); ?>

</div>

<script src="https://js.pusher.com/3.1/pusher.min.js"></script>
<script>
 
// Enable pusher logging - don't include this in production
Pusher.logToConsole = true;

var pusher = new Pusher('ce901ef053e354fcd965', {
  encrypted: true
});

var channel = pusher.subscribe('test_channel');
channel.bind('my_event', function(data) {
// alert(data.message);
var message = data.message;
toastr.info(message);
});
</script>
