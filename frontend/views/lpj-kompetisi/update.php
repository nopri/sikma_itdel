<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LpjKompetisi */

$this->title = 'Update LPJ: ' . $model->id_LpjKompetisi;
$this->params['breadcrumbs'][] = ['label' => 'Kompetisi', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_LpjKompetisi, 'url' => ['view', 'id' => $model->id_LpjKompetisi]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lpj-kompetisi-update">
	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
