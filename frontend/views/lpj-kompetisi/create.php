<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LpjKompetisi */

$this->title = 'Upload LPJ Kompetisi';
$this->params['breadcrumbs'][] = ['label' => 'LPJ Kompetisi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lpj-kompetisi-create">
	<h1><?= Html::encode($this->title) ?>
	
</h1>

<?= $this->render('_form', [
	'model' => $model,
]) ?>

</div>
