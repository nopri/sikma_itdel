 <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="widget">
                            <h5 class="widgetheading">Sistem Informasi Kemahasiswaan</h5>
                            <address>
                    
                            <p>
                                <i class="icon-envelope-alt"></i> Jhosuagaruda389@gmail.com<br>
                                <i class="icon-envelope-alt"></i> SwetaHutauruk@gmail.com<br>
                                <i class="icon-envelope-alt"></i> NopriSiahaan@gmail.com
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="widget">
                            <h5 class="widgetheading">Our Social Media</h5>
                            <address>
                    
                            <ul class="social-network">
								<li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
								<li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
							</ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="widget">
                            <h5 class="widgetheading">contact Us</h5>
                            <ul class="link-list">
                                <li><a href="#">Institut Teknologi Del</a></li>
                                <li><a href="#">@del.ac.id</a></li>
                                <li><a href="#">Jl. Sisingamangaraja, Sitoluama</a></li>
                                <li><a href="#">Sumatera Utara, Indonesia</a></li>
                                <li><a href="#">Kode Pos: 22381</a></li>
                                <li><a href="#">Telp: +62 632 331234</a></li>	
                                <li><a href="#">Fax: +62 632 331116</a></li>





                            </ul>
                        </div>
                    </div>
                    
            </div>
            
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="copyright">
                                
                                <div class="credits" style="position: center">
                                    <!--
                    
                  -->
                                    Designed by <a>&copy;Kelompok TA 08</a>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
         
        </footer>
    </div>
    <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
    <!-- javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->