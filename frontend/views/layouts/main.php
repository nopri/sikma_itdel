<style>

    #dropdown-menu{
        height: 300px;

        overflow: scroll;
    }



</style>

<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\models\Notifikasi;
use common\widgets\Alert;
use rmrevin\yii\fontawesome\FA;

rmrevin\yii\fontawesome\AssetBundle::register($this);
AppAsset::register($this);

if (!Yii::$app->user->isGuest) {
    $notifs = Notifikasi::find(['id_mhs'=>Yii::$app->user->identity->id])->orderBy(['status'=>SORT_ASC])->all();
    
    
    $arrNotif = "";
    $notifLen = 0; 
    foreach($notifs as $notif){
        if ($notif['status'] == 0) {
            $notifLen++;
            $arrNotif .= '<button style=" background-color: transparent;border-color: transparent;" class ="btn fa fa-close" onclick="deleteNotif(this.value)" value="'.$notif['notif_id'].'"></button><li class=""><a href="#">'.$notif['desc'].'</li></a>';

            
            
            
        } else if ($notif['status']!=100) {
            $arrNotif .= '<a href="#">'.$notif['desc'].'</a></li>';
        }

    }
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>

    <div class="wrap">
        <?php
        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [

                'innerContainerOptions' => ['class' => 'container-fluid'],
            ],
        ]);
        $menuItems = [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Prestasi', 'url' => ['/prestasi']],  
            ['label' => 'Kegiatan', 'url' => ['/'],

            'items' => [
                ['label' => 'Request Kegiatan', 'icon' => 'file-text', 'url' => ['/kegiatan']],
                ['label' => 'LPJ Kegiatan', 'icon' => 'file-text', 'url' => ['/l-p-j-kegiatan']],
                ['label' => 'Booking Room', 'icon' => 'file-text', 'url' => ['/ruangan']],
            ]],
            
            
            ['label' => 'Beasiswa', 'url' => ['/beasiswa']],  
            ['label' => 'Kompetisi', 'url' => ['/'],

            'items' => [
                ['label' => 'Request Kompetisi', 'icon' => 'file-text', 'url' => ['/kompetisi']],
                ['label' => 'LPJ Kompetisi', 'icon' => 'file-text', 'url' => ['/lpj-kompetisi']],
            ],
        ],
    ];

    if (Yii::$app->user->isGuest) {
        // $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
    
        $menuItems[] = '<li>'
        . Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(
            'Logout (' . Yii::$app->user->identity->username . ')',
            ['class' => 'btn btn-link logout']
        )
        . Html::endForm()
        . '</li>';
        $menuItems[] = '<ul class="nav navbar-nav navbar-right">
        <span class="badge badge-pill badge-danger" style="float:right;margin-bottom:-10px;">
        
        '.$notifLen.'
        </span>
        <li class="dropdown">


        <a href="#" class="dropdown-toggle" data-toggle="dropdown" ><span class="label label-pill label-danger count" style="border-radius:10px;"></span> <span class="glyphicon glyphicon-bell" style="font-size:18px;"></span></a>
        <ul class="dropdown-menu">
        <li class="header"><b>Notifikasi</b></li>
        '.$arrNotif.'


        </ul>
        </li>
        </ul>';
        
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?php /* Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) */ ?>
        <?= Alert::widget() ?>
        <?=$this->render('header')?>
        <?= $content ?>
        <?=$this->render('footer')?>

    </div>
</div>

<<<<<<< frontend/views/layouts/main.php
<!-- <footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

   
    </div>
</footer> -->
=======

>>>>>>> frontend/views/layouts/main.php

<script>
    function deleteNotif(param) {
        location.href = "http://localhost/D3TI_08/sikma_new_del//frontend/web/index.php?r=notifikasi%2Fdeletenotif&id="+param;
    }
</script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
