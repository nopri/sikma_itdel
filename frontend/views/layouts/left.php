<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Prestasi', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    ['label' => 'Kompetisi', 'icon' => 'dashboard', 'url' => ['/debug'],
                    'items' => [
                        ['label' => 'Izin Kompetisi', 'icon' => 'circle-o', 'url' => '#',],
                        [
                            'label' => 'LPJ Kompetisi',
                            'icon' => 'circle-o',
                            'url' => '#',
                        ],
                    ],
                ],

                ['label' => 'Kegiatan', 'icon' => 'dashboard', 'url' => ['/debug'],
                'items' => [
                    ['label' => 'Izin Kegiatan', 'icon' => 'circle-o', 'url' => '#',],
                    [
                        'label' => 'LPJ Kegiatan',
                        'icon' => 'circle-o',
                        'url' => '#',
                    ],
                ],
            ],


            ['label' => 'Beasiswa', 'icon' => 'dashboard', 'url' => ['/debug'],
            
        ],
    ]
]
) ?>

</section>

</aside>

