<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use backend\models\BeasiswaCompany;
/* @var $this yii\web\View */
/* @var $model app\models\Prestasi */
/* @var $form yii\widgets\ActiveForm */
$asset = frontend\assets\AppAsset::register($this);
$baseUrl = $asset->baseUrl;
?>

<div class="prestasi-form">

  <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>


  

  <div class="row">
    <div class="col-md-8 col-sm-6">
     <?= $form->field($model, 'jenis_prestasi')->textInput(['maxlength' => true]) ?>
   </div>
 </div>

 
 <div class="row">
  <div class="col-md-8 col-sm-8">
    <?= $form->field($model, 'tgl_awal_kegiatan')->widget(DatePicker::className(), [
     'options' => ['placeholder' => 'Masukkan tanggal'],
     'pluginOptions' => [
       'autoclose' => true,
       'format' => 'yy/mm/dd'
     ]

   ]);?>
 </div>
</div>

<div class="row">
  <div class="col-md-8 col-sm-8">
    <?= $form->field($model, 'tgl_akhir_kegiatan')->widget(DatePicker::className(), [
     'options' => ['placeholder' => 'Masukkan tanggal'],
     'pluginOptions' => [
       'autoclose' => true,
       'format' => 'yy/mm/dd'
     ]

   ]);?>

 </div>
</div>


<?= $form->field($model, 'upload_file')->fileInput() ?>


<div class="row">
  <div class="col-md-8 col-sm-8">
    <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>
  </div>
</div>
<div class="form-group">
  <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Selesai', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

  <?= Html::a('Batal', ['index'], ['class' => 'btn btn-danger']) ?>
</div>

<?php ActiveForm::end(); ?>

</div>
