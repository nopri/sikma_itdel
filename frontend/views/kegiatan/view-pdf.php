<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Kegiatan */

$this->title = 'Data Kegiatan';



\yii\web\YiiAsset::register($this);
?>
<div class="kegiatan-view">

	


	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'jenis_kegiatan',
			['attribute' => 'upload_proposal',
			'label' => 'Proposal Kegiatan',
			'format' => 'html',
			'value' => 
			function($model){
				return Html::a($model->upload_proposal, ['kegiatan/download', 'id'=> $model->id_kegiatan]);
			}
		],
		['label' => 'Status Request', 'value' => $model->statusRequest->name],
	],



]) ?>



	<?= Html::a('Lihat Pdf ', [
    'pdf',
    'id' => $model->upload_proposal,
], [
    'class' => 'btn btn-primary',
    'target' => '_blank',
]); ?>

</div>
