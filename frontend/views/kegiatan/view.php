<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Kegiatan */

$this->title = 'Data Kegiatan';
$this->params['breadcrumbs'][] = ['label' => ' Kegiatan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


\yii\web\YiiAsset::register($this);
?>
<div class="kegiatan-view">

	<h1><?= Html::encode($this->title) ?></h1>


	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'jenis_kegiatan',
			['attribute' => 'upload_proposal',
			'label' => 'Proposal Kegiatan',
			'format' => 'html',
			'value' => 
			function($model){
				return Html::a($model->upload_proposal, ['kegiatan/download', 'id'=> $model->id_kegiatan]);
			}
		],
		['label' => 'Status Request', 'value' => $model->statusRequest->name],



		
	],



]) ?>
<?php 

echo Html::a('<i class="fa far fa-hand-point-up"></i> Privacy Statement', ['/view-privacy'], [
    'class'=>'btn btn-danger', 
    'target'=>'_blank', 
    'data-toggle'=>'tooltip', 
    'title'=>'Will open the generated PDF file in a new window'
]);
	
 ?>
	

</div>
