<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\Ruangan */
/* @var $form yii\widgets\ActiveForm */
$asset = frontend\assets\AppAsset::register($this);
$baseUrl = $asset->baseUrl;
?>

<div class="ruangan-form">

  <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

  <div class="row">
    <div class="col-md-8 col-sm-8">
        <?= $form->field($model, 'jns_ruangan')->textInput(['maxlength' => true]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-8 col-sm-8">
        <?= $form->field($model, 'tujuan')->textInput(['maxlength' => true]) ?>
    </div>
</div>


<div class="row">
    <div class="col-md-8 col-sm-8">
        <?= $form->field($model, 'waktu_peminjaman')->widget(DateTimePicker::classname(), [
            'options' => ['placeholder' => 'Enter event time ...'],
            'pluginOptions' => [
                'autoclose' => true
            ]
        ]);?>

    </div>
</div>

<div class="row">
    <div class="col-md-8 col-sm-8">
        <?= $form->field($model, 'waktu_akhir_peminjaman')->widget(DateTimePicker::classname(), [
            'options' => ['placeholder' => 'Enter event time ...'],
            'pluginOptions' => [
                'autoclose' => true
            ]
        ]);?>

    </div>
</div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Selesai Edit', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

        <?= Html::a('Batal', ['index'], ['class' => 'btn btn-danger']) ?>


    </div>

    <?php ActiveForm::end(); ?>

</div>
