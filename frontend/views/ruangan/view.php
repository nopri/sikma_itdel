<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Ruangan */

$this->title = 'Data Booking Ruangan';
$this->params['breadcrumbs'][] = ['label' => 'Booking Ruangan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ruangan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'jns_ruangan',
            'tujuan',
            'waktu_peminjaman',
            'waktu_akhir_peminjaman',
            ['label' => 'Status Request', 'value' => $model->statusRequest->name],
        ],

    ]) ?>

</div>
