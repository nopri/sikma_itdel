<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\BeasiswaMahasiswaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="beasiswa-mahasiswa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id_beasmaha') ?>
    <?= $form->field($model, 'nama_beasiswa')?>
    <?= $form->field($model, 'jns_beasiswa') ?>

    <?= $form->field($model, 'tnggl_awal') ?>

    <?php // echo $form->field($model, 'tnggl_akhir') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
