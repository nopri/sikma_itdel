<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kompetisi;

/**
 * KompetisiSearch represents the model behind the search form of `frontend\models\Kompetisi`.
 */
class KompetisiSearch extends Kompetisi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
      return [
        [['id_kompetisi', 'id_mhs'], 'integer'],
        [['jenis_kompetisi', 'upload_proposal'], 'safe'],
      ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
      return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
      $query = Kompetisi::find();

        // add conditions that should always apply here

      $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
          'pageSize' => 15,
        ],
      ]);

      $this->load($params);

      if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
        return $dataProvider;
      }

        // grid filtering conditions
      $query->andFilterWhere([
        'id_kompetisi' => $this->id_kompetisi,
        'id_mhs' => $this->id_mhs,
      ]);

      $query->andFilterWhere(['like', 'jenis_kompetisi', $this->jenis_kompetisi])
      ->andFilterWhere(['like', 'upload_proposal', $this->upload_proposal]);

      return $dataProvider;
    }
  }
