<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "notifikasi".
 *
 * @property int $notif_id
 * @property int $id_mhs
 * @property int $desc
 * @property int $status
 *
 * @property NewUser $mhs
 */
class Notifikasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifikasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mhs', 'desc', 'status'], 'required'],
            [['id_mhs', 'desc', 'status'], 'integer'],
            [['id_mhs'], 'exist', 'skipOnError' => true, 'targetClass' => NewUser::className(), 'targetAttribute' => ['id_mhs' => 'id_mhs']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'notif_id' => 'Notif ID',
            'id_mhs' => 'Id Mhs',
            'desc' => 'Desc',
            'status' => 'Status',
        ];
    }

    /**
     * Gets query for [[Mhs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMhs()
    {
        return $this->hasOne(NewUser::className(), ['id_mhs' => 'id_mhs']);
    }
}
