<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lpj_kegiatan".
 *
 * @property int $id_LpjKegiatan
 * @property int $id_mhs
 * @property string $jenis_kegiatan
 * @property string $file_lpj
 */
class LpjKegiatan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lpj_kegiatan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis_kegiatan', 'file_lpj'], 'required'],
            [['jenis_kegiatan'], 'safe'],
            [['file_lpj'], 'file', 'skipOnEmpty' => true, 'extensions'=>'pdf'],
            [['jenis_kegiatan'], 'string', 'max' => 50],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_LpjKegiatan' => 'Id Lpj Kegiatan',
            'id_mhs' => 'Id Mhs',
            'jenis_kegiatan' => 'Jenis Kegiatan',
            'file_lpj' => 'File LPJ',
        ];
    }

    public function getStatusRequest()
    {
        return $this->hasOne(StatusRequest::className(), ['status_request_id' => 'status_request_id']);
    }


    public function getId()
    {
        return $this->hasOne(Id::className(), ['id_mhs' => 'id_mhs']);
    }

}

