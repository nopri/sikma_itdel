<?php

namespace app\models;

use Yii;


class Ruangan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ruangan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jns_ruangan', 'tujuan', 'waktu_peminjaman', 'waktu_akhir_peminjaman'], 'required'],
            [['waktu_peminjaman', 'waktu_akhir_peminjaman'], 'safe'],
            [['jns_ruangan', 'tujuan'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            // 'id_ruangan' => 'Id Ruangan',
            'jns_ruangan' => 'Ruangan',
            'id_mhs' => 'Id Mhs',
            'tujuan' => 'Tujuan',
            'waktu_peminjaman' => 'Waktu Peminjaman',
            'waktu_akhir_peminjaman' => 'Batas Waktu Peminjaman',
        ];
    }

    public function getStatusRequest()
    {
        return $this->hasOne(StatusRequest::className(), ['status_request_id' => 'status_request_id']);
    }
}
