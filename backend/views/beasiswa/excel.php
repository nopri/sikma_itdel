<?php

\backend\assets\web\php\ExcelGrid::Widget([
	'dataProvider'=> $dataProvider,
	'filterModel' => $searchModel,
	'filename'	  => 'Beasiswa',
	'properties'  => [],
	'columns'	 => [
		['class' =>  'yii\grid\SerialColumn'],
		'nama_mhs',
		'alamat',
		'nim_mhs',
		'jlh_bersaudara',
		'anak_ke',
		'tanggungan',
		'pekerjaan_ayah',
		'pekerjaan_ibu',
		'penghasilan_ayah',
		'penghasilan_ibu',

		['attribute' => 'status_request_id',
		'label' => 'Status Request',
		'value' => function($model){
			if(is_null($model->statusRequest['name'])){
				return '-';
			}else{
				return $model->statusRequest['name'];
			}
		}
	],
]
])
?>