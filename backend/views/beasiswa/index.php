<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\components\ToolsColumn;
use common\helpers\LinkHelper;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\BeasiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Beasiswa';
$this->params['breadcrumbs'][] = $this->title;
$uiHelper=\Yii::$app->uiHelper;
?>

<div class="beasiswa-index">
<h1><?= Html::encode($this->title) ?></h1>
<p> <?= Html::a('Export ke Excel', ['excel'], ['class' => 'btn btn-primary']) ?> 
</p>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],
      'nama_mhs',
      'nim_mhs',
      'alamat',
      // 'jlh_bersaudara',
      // 'anak_ke',
      // 'tanggungan',
      // 'pekerjaan_ayah',
      // 'pekerjaan_ibu',
      // 'penghasilan_ayah',
      // 'penghasilan_ibu',
      ['attribute' => 'status_request_id',
      'label' => 'Status Request',
      'value' => function($model){
        if(is_null($model->statusRequest['name'])){
          return '-';
      }else{
          return $model->statusRequest['name'];
      }
  }
],


['class' => 'common\components\ToolsColumn',
'template' => '{view} {approve} {reject}',
'header' => 'Aksi',
'buttons' => [
    'view' => function ($url, $model){
        return ToolsColumn::renderCustomButton($url, $model, 'View Detail', 'fa fa-eye');
    },
    'reject' => function ($url, $model){
        if ($model->status_request_id == 1 || $model->status_request_id == 2 ) {
            return "";
        }else{
            return ToolsColumn::renderCustomButton($url, $model, 'Reject', 'fa fa-times');
        }
    },
    'approve' => function ($url, $model){
        if ($model->status_request_id == 1 || $model->status_request_id == 2 ) {
            return "";
        }else{
            return ToolsColumn::renderCustomButton($url, $model, 'Approve', 'fa fa-check');
        }
    },
],
],
],

]); ?>

</div>
