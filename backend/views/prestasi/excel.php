<?php
use backend\models\NewUser;

\backend\assets\web\php\ExcelGrid::Widget([
	'dataProvider'=> $dataProvider,
	'filterModel' => $searchModel,
	'filename'	  => 'Prestasi',
	'properties'  => [

	],
	'columns'	 => [
		['class' =>  'yii\grid\SerialColumn'],
		['attribute' => 'firstname',
		'label' => 'Nama Mahasiswa',
		'value' => function ($data) {
			return NewUser::findOne(['id_mhs'=>$data->id_mhs])->username;
		},
	],

	'jenis_prestasi',
	'tgl_awal_kegiatan',
	'tgl_akhir_kegiatan',
	'keterangan',

	['attribute' => 'status_request_id',
	'label' => 'Status Request',
	'value' => function($model){
		if(is_null($model->statusRequest['name'])){
			return '-';
		}else{
			return $model->statusRequest['name'];
		}
	}
],
]
])
?>