<?php
use yii\helpers\Html;
use yii\widgets\ActiverForm;

?>
<div class="prestasi-form">

<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'id_prestasi')->textInput()?>
<?= $form->field($model, 'jenis_prestas')->textInput(['maxlength' => true]) ?>

    <<?= $form->field($model, 'upload_proposal')->fileInput() ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>