<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use common\components\ToolsColumn;
use common\helpers\LinkHelper;
use backend\models\NewUser;

/* @var $this yii\web\View */
/* @var $model backend\modules\kmhs\models\KegiatanMahasiswa */
$this->title = 'Data Prestasi Mahasiswa';
?>
<div class="prestasi-view">
  <h1><?= Html::encode($this->title) ?></h1>
  <?= DetailView::widget([
    'model' => $model,
    'attributes' => [

      ['label'=>'Nama Mahasiswa',
      'value' => function ($data) {
        return NewUser::findOne(['id_mhs'=>$data->id_mhs])->username;
      },
    ],

    'jenis_prestasi',
    'tgl_awal_kegiatan',
    'tgl_akhir_kegiatan',
    'keterangan',
    ['attribute' => 'upload_file',
    'label' => 'File Prestasi',
    'format' => 'html',
    'value' => 
    function($model){
      return Html::a($model->upload_file, ['prestasi/download', 'id'=> $model->id_prestasi]);
    }
  ],
  ['label' => 'Status Request', 'value' => $model->statusRequest->name],
],
]) ?>

  <?= Html::a('Lihat Pdf', [
    'pdf',
    'id' => $model->id_prestasi,
  ], [
    'class' => 'btn btn-primary',
    'target' => '_blank',
  ]); ?>

</div>
