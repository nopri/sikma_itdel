<?php

use yii\widgets\DetailView;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\components\ToolsColumn;
use common\helpers\LinkHelper;
use backend\models\NewUser;

/* @var $this yii\web\View */
/* @var $model backend\modules\kmhs\models\KegiatanMahasiswa */
$this->title = 'Data Kompetisi';
?>
<div class="kompetisi-view">
  <h1><?= Html::encode($this->title) ?></h1>
  <?= DetailView::widget([
    'model' => $model,
    'attributes' => [

      ['label'=>'Nama Mahasiswa',
      'value' => function ($data) {
        return NewUser::findOne(['id_mhs'=>$data->id_mhs])->username;
      },
    ],

    'jenis_kompetisi',
    ['attribute' => 'upload_proposal',
    'label' => 'Proposal Kompetisi',
    'format' => 'html',
    'value' => 
    function($model){
      return Html::a($model->upload_proposal, ['kompetisi/download', 'id'=> $model->id_kompetisi]);
    }
  ],
  ['label' => 'Status Request', 'value' => $model->statusRequest->name],
],
]) ?>
  <?= Html::a('Lihat Pdf', [
    'pdf',
    'id' => $model->id_kompetisi,
  ], [
    'class' => 'btn btn-primary',
    'target' => '_blank',
  ]); ?>
</div>
