<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LpjKompetisi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lpj-kompetisi-form">
	<?php $form = ActiveForm::begin(); ?>
	<?= $form->field($model, 'id_LpjKompetisi')->textInput()?>
	<?= $form->field($model, 'jenis_kompetisi')->textInput(['maxlength' => true]) ?>

	<<?= $form->field($model, 'file_lpj')->fileInput() ?>

<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>