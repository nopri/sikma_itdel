<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\Ruangan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ruangan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_kegiatan')->textInput() ?>

    <?= $form->field($model, 'jns_ruangan')->textInput(['maxlength' => true]) ?>

     <?= $form->field($model, 'jns_ruangan')->dropDownList(
        ArrayHelper::map(Companies::find()->all(),'company_id','company_name'),
        ['prompt'=>'Select beasiswa']
    ) ?>

    <?= $form->field($model, 'tujuan')->textInput(['maxlength' => true]) ?>
    

    <?= $form->field($model, 'wkt_peminjaman')->widget(\yii\jui\DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'MM-dd-yyyy',
    ]) ?>




    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
