<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use common\components\ToolsColumn;
use common\helpers\LinkHelper;
use backend\models\NewUser;

/* @var $this yii\web\View */
/* @var $model backend\modules\kmhs\models\KegiatanMahasiswa */
$this->title = 'Data Kegiatan';
?>
<div class="kegiatan-view">
  <h1><?= Html::encode($this->title) ?></h1>
  <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
      
     ['label'=>'Nama Mahasiswa',
     'value' => function ($data) {
      return NewUser::findOne(['id_mhs'=>$data->id_mhs])->username;
    },
  ],
  'jenis_kegiatan',
  ['attribute' => 'upload_proposal',
  'label' => 'Proposal Kegiatan',
  'format' => 'html',
  'value' => 
  function($model){
    return Html::a($model->upload_proposal, ['kegiatan/download', 'id'=> $model->id_kegiatan]);
  }
],
['label' => 'Status Request', 'value' => $model->statusRequest->name],
],
]) ?>

  <?= Html::a('Lihat Pdf', [
    'pdf',
    'id' => $model->id_kegiatan,
  ], [
    'class' => 'btn btn-primary',
    'target' => '_blank',
  ]); ?>

</div>
