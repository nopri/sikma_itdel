<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\BeasiswaMahasiswa */

// $this->title = $model->id_beasmaha;
$this->title = 'Informasi Beasiswa';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Beasiswa Mahasiswa'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="beasiswa-mahasiswa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_beasmaha], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id_beasmaha',
            // 'id_beasiswa',
            // 'id_mhs',
            'jns_beasiswa',
            'informasi_beasiswa',
            'tnggl_awal',
            'tnggl_akhir',
            'file_beasiswa',
        ],
    ]) ?>

</div>
