<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\components\ToolsColumn;
use common\helpers\LinkHelper;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\BeasiswaMahasiswaSearch */
/* @var $dataProvider yii\dat
a\ActiveDataProvider */



$this->title = Yii::t('app', 'Beasiswa Mahasiswas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beasiswa-mahasiswa-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Beasiswa Mahasiswa'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Export ke Excel', ['excel'], ['class' => 'btn btn-primary']) ?> 
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_beasmaha',
            // 'id_beasiswa',
            // 'id_mhs',
            'jns_beasiswa',
            'informasi_beasiswa',
            'tnggl_awal',
            'tnggl_akhir',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
