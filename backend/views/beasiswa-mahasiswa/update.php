<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BeasiswaMahasiswa */

$this->title = Yii::t('app', 'Update Beasiswa Mahasiswa: {name}', [
    'name' => $model->id_beasmaha,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Beasiswa Mahasiswas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_beasmaha, 'url' => ['view', 'id' => $model->id_beasmaha]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="beasiswa-mahasiswa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<script type="text/javascript">
    var selectField = document.getElementById("beasiswamahasiswa-jns_beasiswa");
    var hiddenField = document.getElementById("dynamicmodel-jns_beasiswa");
    
    selectField.addEventListener("change", checkOption);

    function checkOption() {
        if (selectField.options[selectField.selectedIndex].value == "Lainnya") {
            hiddenField.setAttribute("type", "text");
        }
    }
</script>