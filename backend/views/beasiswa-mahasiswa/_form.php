<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use backend\models\BeasiswaCompany;

/* @var $this yii\web\View */
/* @var $model backend\models\BeasiswaMahasiswa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="beasiswa-mahasiswa-form" id="test">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-md-6 col-sm-6">
           <?= $form->field($model, 'jns_beasiswa')->dropDownList(
            ArrayHelper::map(BeasiswaCompany::find()->all(),'jenis_beasiswa','jenis_beasiswa'),
            ['prompt'=>'Select beasiswa'],
            ['id' => 'jenis']
        ) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-sm-6">
        <?= $form->field($model, 'informasi_beasiswa')->textarea(['maxlength' => true]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <?= $form->field($beasiswaLainnyaModel, 'jns_beasiswa')->hiddenInput(['placeholder' => "Mohon isikan nama beasiswa tersebut di sini"])->label(false) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-sm-6">
        <?= $form->field($model, 'tnggl_awal')
        ->widget(DatePicker::className(), [
        // inline too, not bad
           'inline' => false, 
         // modify template for custom rendering
        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
           'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ])?>
</div>
</div>




<div class="row">
    <div class="col-md-6 col-sm-6">
        <?= $form->field($model, 'tnggl_akhir')
        ->widget(DatePicker::className(), [
        // inline too, not bad
           'inline' => false, 
         // modify template for custom rendering
        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
           'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ])?>
</div>
</div>
<?= $form->field($model, 'file_beasiswa')->fileInput() ?>

<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>

</div>


<script type="text/javascript">
    var selectField = document.getElementById("beasiswamahasiswa-jns_beasiswa");
    var hiddenField = document.getElementById("dynamicmodel-jns_beasiswa");
    
    selectField.addEventListener("change", checkOption);

    function checkOption() {
        if (selectField.options[selectField.selectedIndex].value == "Lainnya") {
            hiddenField.setAttribute("type", "text");
        }
    }
</script>