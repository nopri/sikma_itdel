<?php

use dosamigos\highcharts\HighCharts;

   //use miloschuman\highcharts\Highcharts;
/* @var $this yii\web\View */

$this->title = 'Grafik Prestasi Mahasiswa';

foreach($dgrafik as $values){
   $a[0]= ($values['jenis_prestasi']);
   $c[]= ($values['jenis_prestasi']);
   $b[]= array('type'=> 'column', 'name' =>$values['jenis_prestasi'], 'data' => array((int)$values['jml']));
}
echo
Highcharts::widget([
   'clientOptions' => [
      'chart'=>[
         'type'=>'pie'
      ],
      'title' => ['text' => 'Grafik Prestasi Mahasiswa Berdasarkan Jenis Prestasi'],
      'xAxis' => [
         'categories' => ['Jenis Prestasi']
      ],
      'yAxis' => [
         'title' => ['text' => 'Jumlah Prestasi Mahasiswa']
      ],
      'series' => $b
   ]
]);