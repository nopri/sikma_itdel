<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Notifikasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notifikasi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_mhs')->textInput() ?>

    <?= $form->field($model, 'desc')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
