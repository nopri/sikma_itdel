<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\ProgramStudi;

/* @var $this yii\web\View */
/* @var $model frontend\models\MhsResign */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mhs-resign-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="row">
        <div class="col-md-8 col-sm-8">
            <?= $form->field($model, 'nama_mhs')->textInput(['maxlength' => true]) ?>
        </div>
    </div>



    <div class="row">
        <div class="col-md-8 col-sm-8">
            <?= $form->field($model, 'nim')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-sm-8">
            <?= $form->field($model, 'prodi')->dropDownList(
                ArrayHelper::map(ProgramStudi::find()->all(),'program_studi','program_studi'),
                ['prompt'=>'Pilih Program Studi']
            ) ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-8 col-sm-8">
            <?= $form->field($model, 'alasan')->textInput(['maxlength' => true]) ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Selesai Edit', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

        <?= Html::a('Batal', ['index'], ['class' => 'btn btn-danger']) ?>


    </div>

    <?php ActiveForm::end(); ?>
</div>