<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MhsResign */

$this->title = 'Update Data Mahasiswa';
$this->params['breadcrumbs'][] = ['label' => 'Mahasiswa Resign', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_resign, 'url' => ['view', 'id' => $model->id_resign]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mhs-resign-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
