<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\components\ToolsColumn;
use common\helpers\LinkHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MhsResignSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Mahasiswa Resign';
$this->params['breadcrumbs'][] = $this->title;
$uiHelper=\Yii::$app->uiHelper;
?>
<div class="mhs-resign-index">

  <h1><?= Html::encode($this->title) ?></h1>
  <p> <?= Html::a('Tambah Mahasiswa', ['create'], ['class' => 'btn btn-success']) ?> 
  <?= Html::a('Export ke Excel', ['excel'], ['class' => 'btn btn-primary']) ?>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],
      'nama_mhs',
      'nim',
      'prodi',
      'alasan',
      
      ['class' => 'common\components\ToolsColumn',
      'template' => '{view}',
      'header' => 'Aksi',
      'buttons' => [
        'view' => function ($url, $model){
          return ToolsColumn::renderCustomButton($url, $model, 'View Detail', 'fa fa-eye');
        },
      ],
    ],
  ],

]); ?>

</div>