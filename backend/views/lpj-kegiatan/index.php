<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\components\ToolsColumn;
use common\helpers\LinkHelper;
use backend\models\NewUser;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\LpjKegiatanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'LPJ Kegiatan');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lpj-kegiatan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            
            'jenis_kegiatan',
            ['attribute' => 'file_lpj',
            'label' => 'LPJ Kegiatan',
            'format' => 'html',
            'value' => 
            function($model){
                return Html::a($model->file_lpj, ['lpj-kegiatan/download', 'id'=> $model->id_LpjKegiatan]);
            }
        ],
        ['attribute' => 'status_request_id',
        'label' => 'Status Request',
        'value' => function($model){
            if(is_null($model->statusRequest['name'])){
                return '-';
            }else{
                return $model->statusRequest['name'];
            }
        }
    ],

    ['class' => 'common\components\ToolsColumn',
    'template' => '{view} {approve} {reject}',
    'header' => 'Aksi',
    'buttons' => [
        'view' => function ($url, $model){
            return ToolsColumn::renderCustomButton($url, $model, 'View Detail', 'fa fa-eye');
        },
        'reject' => function ($url, $model){
            if ($model->status_request_id == 1 || $model->status_request_id == 2 ) {
                return "";
            }else{
                return ToolsColumn::renderCustomButton($url, $model, 'Reject', 'fa fa-times');
            }
        },
        'approve' => function ($url, $model){
            if ($model->status_request_id == 1 || $model->status_request_id == 2 ) {
                return "";
            }else{
                return ToolsColumn::renderCustomButton($url, $model, 'Approve', 'fa fa-check');
            }
        },
    ],
],
],

]); ?>

</div>
