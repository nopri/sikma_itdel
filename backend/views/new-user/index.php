<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\ToolsColumn;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\NewUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Mahasiswa';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="new-user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Tambah Mahasiswa', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id_mhs',
            'authKey',
            'firstname',
            'lastname',
            'username',
            'password',

            ['class' => 'common\components\ToolsColumn',
            'template' => '{view}',
            'header' => 'Aksi',
            'buttons' => [
                'view' => function ($url, $model){
                  return ToolsColumn::renderCustomButton($url, $model, 'View Detail', 'fa fa-eye');
              },
          ],
      ],
  ],
]); ?>


</div>
