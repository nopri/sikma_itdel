<?php

namespace backend\modules\kmhs\models;

use Yii;

/**
 * This is the model class for table "pengumuman".
 *
 * @property int $id_informasi
 * @property string $gambar
 * @property string $deskripsi
 */
class Pengumuman extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pengumuman';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_informasi', 'gambar', 'deskripsi'], 'required'],
            [['id_informasi'], 'integer'],
            [['gambar', 'deskripsi'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_informasi' => 'Id Informasi',
            'gambar' => 'Gambar',
            'deskripsi' => 'Deskripsi',
        ];
    }
}
