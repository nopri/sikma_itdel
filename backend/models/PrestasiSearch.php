<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Prestasi;

/**
 * PrestasiSearch represents the model behind the search form of `backend\models\Prestasi`.
 */
class PrestasiSearch extends Prestasi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_prestasi'], 'integer'],
            [['jenis_prestasi',  'tgl_awal_kegiatan', 'tgl_akhir_kegiatan', 'upload_file', 'keterangan'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function searchExcel($params)
    {
        $query = Prestasi::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

         // grid filtering conditions
         $query->andFilterWhere([

            'tgl_awal_kegiatan' => $this->tgl_awal_kegiatan,
            'tgl_akhir_kegiatan' => $this->tgl_akhir_kegiatan,
        ]);

        $query->andFilterWhere(['like', 'jenis_prestasi', $this->jenis_prestasi])
            ->andFilterWhere(['like', 'upload_file', $this->upload_file])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }

    public function search($params)
    {
        $query = Prestasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([

            'tgl_awal_kegiatan' => $this->tgl_awal_kegiatan,
            'tgl_akhir_kegiatan' => $this->tgl_akhir_kegiatan,
        ]);

        $query->andFilterWhere(['like', 'jenis_prestasi', $this->jenis_prestasi])
            ->andFilterWhere(['like', 'upload_file', $this->upload_file])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}