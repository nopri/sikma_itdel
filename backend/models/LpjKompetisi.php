<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "lpj_kompetisi".
 *
 * @property int $id_LpjKompetisi
 * @property int $id_mhs
 * @property string $jenis_kompetisi
 * @property string $file_lpj
 * @property int $status_request_id
 */
class LpjKompetisi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lpj_kompetisi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mhs', 'jenis_kompetisi','file_lpj', 'status_request_id'], 'required'],
            [['id_mhs', 'status_request_id'], 'integer'],
            [['jenis_kompetisi'], 'string', 'max' => 50],
            [['file_lpj'], 'file', 'skipOnEmpty' => TRUE,'extensions'=>'pdf']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_LpjKompetisi' => 'Id Lpj Kompetisi',
            'id_mhs' => 'Id Mhs',
            'jenis_kompetisi' => 'Jenis Kompetisi',
            'file_lpj' => 'LPJ Kompetisi',
        ];
    }
     public function getStatusRequest()
    {
        return $this->hasOne(StatusRequest::className(), ['status_request_id' => 'status_request_id']);
    }

    
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_mhs' => 'id_mhs']);
    }
}
