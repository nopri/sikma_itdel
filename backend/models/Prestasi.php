<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "prestasi".
 *
 * @property int $id_prestasi
 * @property int $id_mhs
 * @property string $jenis_prestasi
 * @property string $tgl_awal_kegiatan
 * @property string $tgl_akhir_kegiatan
 * @property string $upload_file
 * @property string $keterangan
 * @property int $status_request_id
 *
 * @property StatusRequest $statusRequest
 */
class Prestasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prestasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mhs', 'jenis_prestasi', 'tgl_awal_kegiatan', 'tgl_akhir_kegiatan', 'upload_file', 'keterangan', 'status_request_id'], 'required'],
            [['id_mhs', 'status_request_id'], 'integer'],
            [['tgl_awal_kegiatan', 'tgl_akhir_kegiatan'], 'safe'],
            [['jenis_prestasi', 'upload_file', 'keterangan'], 'string', 'max' => 255],
            [['status_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatusRequest::className(), 'targetAttribute' => ['status_request_id' => 'status_request_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_prestasi' => 'Id Prestasi',
            'id_mhs' => 'Id Mhs',
            'jenis_prestasi' => 'Jenis Prestasi',
            'tgl_awal_kegiatan' => 'Tgl Awal Kegiatan',
            'tgl_akhir_kegiatan' => 'Tgl Akhir Kegiatan',
            'upload_file' => 'Upload File',
            'keterangan' => 'Lokasi',
            'status_request_id' => 'Status Request ID',
        ];
    }

    /**
     * Gets query for [[StatusRequest]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatusRequest()
    {
        return $this->hasOne(StatusRequest::className(), ['status_request_id' => 'status_request_id']);
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_mhs' => 'id_mhs']);
    }
}
