<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "program_studi".
 *
 * @property string $program_studi
 */
class ProgramStudi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'program_studi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prodi_id', 'program_studi'], 'required'],
            [['program_studi'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'program_studi' => 'Program Studi',
        ];
    }
}
