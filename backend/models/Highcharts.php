<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "highcharts".
 *
 * @property int $id_highcharts
 */
class Highcharts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'highcharts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_highcharts' => 'Id Highcharts',
        ];
    }
}
