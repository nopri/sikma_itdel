<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Beasiswa;

/**
 * BeasiswaSearch represents the model behind the search form of `app\models\Beasiswa`.
 */
class BeasiswaSearch extends Beasiswa
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_beasiswa', 'id_mhs'], 'integer'],
            [['nama_mhs', 'alamat', 'nim_mhs', 'jlh_bersaudara', 'anak_ke', 'tanggungan', 'pekerjaan_ayah', 'pekerjaan_ibu', 'penghasilan_ayah', 'penghasilan_ibu', 'status_request_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */


    public function searchExcel($params)
    {
        $query = Beasiswa::find()->where(['status_request_id' => 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
         // grid filtering conditions
         $query->andFilterWhere([
            'id_beasiswa' => $this->id_beasiswa,
            'id_mhs' => $this->id_mhs,
        ]);

        $query->andFilterWhere(['like', 'nama_mhs', $this->nama_mhs])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'nim_mhs', $this->nim_mhs])
            ->andFilterWhere(['like', 'jlh_bersaudara', $this->jlh_bersaudara])
            ->andFilterWhere(['like', 'anak_ke', $this->anak_ke])
            ->andFilterWhere(['like', 'tanggungan', $this->tanggungan])
            ->andFilterWhere(['like', 'pekerjaan_ayah', $this->pekerjaan_ayah])
            ->andFilterWhere(['like', 'pekerjaan_ibu', $this->pekerjaan_ibu])
            ->andFilterWhere(['like', 'penghasilan_ayah', $this->penghasilan_ayah])
            ->andFilterWhere(['like', 'penghasilan_ibu', $this->penghasilan_ibu])
            ->andFilterWhere(['like', 'status_request_id', $this->status_request_id]);

        return $dataProvider;
    }

    public function search($params)
    {
        $query = Beasiswa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'pagination' => [
                'pageSize' => 15,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_beasiswa' => $this->id_beasiswa,
            'id_mhs' => $this->id_mhs,
        ]);

        $query->andFilterWhere(['like', 'nama_mhs', $this->nama_mhs])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'nim_mhs', $this->nim_mhs])
            ->andFilterWhere(['like', 'jlh_bersaudara', $this->jlh_bersaudara])
            ->andFilterWhere(['like', 'anak_ke', $this->anak_ke])
            ->andFilterWhere(['like', 'tanggungan', $this->tanggungan])
            ->andFilterWhere(['like', 'pekerjaan_ayah', $this->pekerjaan_ayah])
            ->andFilterWhere(['like', 'pekerjaan_ibu', $this->pekerjaan_ibu])
            ->andFilterWhere(['like', 'penghasilan_ayah', $this->penghasilan_ayah])
            ->andFilterWhere(['like', 'penghasilan_ibu', $this->penghasilan_ibu])
            ->andFilterWhere(['like', 'status_request_id', $this->status_request_id]);

        return $dataProvider;
    }
}
