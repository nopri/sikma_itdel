<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "kompetisi".
 *
 * @property int $id_kompetisi
 * @property int $id_mhs
 * @property string $jenis_kompetisi
 * @property string $upload_proposal
 * @property int $status_request_id
 */
class Kompetisi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kompetisi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mhs', 'jenis_kompetisi', 'upload_proposal', 'status_request_id'], 'required'],
            [['id_mhs', 'status_request_id'], 'integer'],
            [['jenis_kompetisi', 'upload_proposal'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kompetisi' => 'Id Kompetisi',
            'id_mhs' => 'Id Mhs',
            'jenis_kompetisi' => 'Jenis Kompetisi',
            'upload_proposal' => 'Upload Proposal',
            'status_request_id' => 'Status Request ID',
        ];
    }

    public function getStatusRequest()
    {
        return $this->hasOne(StatusRequest::className(), ['status_request_id' => 'status_request_id']);
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_mhs' => 'id_mhs']);
    }
}
