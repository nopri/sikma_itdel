<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Ruangan;

/**
 * RuanganSearch represents the model behind the search form of `frontend\models\Ruangan`.
 */
class RuanganSearch extends Ruangan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_ruangan'], 'integer'],
            [['jns_ruangan', 'tujuan', 'waktu_peminjaman', 'waktu_akhir_peminjaman'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ruangan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_ruangan' => $this->id_ruangan,
            //'id_kegiatan' => $this->id_kegiatan,
            'waktu_peminjaman' => $this->waktu_peminjaman,
            'waktu_akhir_peminjaman' => $this->waktu_akhir_peminjaman,
        ]);

        $query->andFilterWhere(['like', 'jns_ruangan', $this->jns_ruangan])
            ->andFilterWhere(['like', 'tujuan', $this->tujuan]);

        return $dataProvider;
    }
}
