<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\LpjKegiatan;

/**
 * LpjKegiatanSearch represents the model behind the search form of `app\models\LpjKegiatan`.
 */
class LpjKegiatanSearch extends LpjKegiatan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_LpjKegiatan', 'id_mhs', 'status_request_id'], 'integer'],
            [['jenis_kegiatan', 'file_lpj'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LpjKegiatan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
              'pagination' => [
                'pageSize' => 15,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_LpjKegiatan' => $this->id_LpjKegiatan,
            'id_mhs' => $this->id_mhs,
            'status_request_id' => $this->status_request_id,
        ]);

        $query->andFilterWhere(['like', 'jenis_kegiatan', $this->jenis_kegiatan])
        ->andFilterWhere(['like', 'file_lpj', $this->file_lpj]);

        return $dataProvider;
    }
}
