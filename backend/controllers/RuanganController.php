<?php 
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\Ruangan;
use backend\models\RuanganSearch;
use backend\models\Notifikasi; 

/**
 * Site controller
 */
class RuanganController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new RuanganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 5];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionApprove($id)
    {
        $model = Ruangan::find()->where(['id_ruangan' => $id])->one();
        $notif = new Notifikasi();
        $notif->id_mhs = $model->id_mhs;
        $notif->desc = "Peminjaman Ruangan Diterima";
        $model->status_request_id=1;
        $searchModel = new RuanganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if($model->save() && $notif->save(false)){
           
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
            
        }
    }

    public function actionReject($id)
    {
        $model = Ruangan::find()->where(['id_ruangan' => $id])->one();
        $notif = new Notifikasi();
        $notif->id_mhs = $model->id_mhs;
        $notif->desc = "Peminjaman Ruangan Ditolak";
        $model->status_request_id=2;
        $searchModel = new RuanganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if($model->save()){
    //disini buat savenya
         if ($notif->save(false)) {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        
    }
}

public function actionCreate()
{
    $model=new Ruangan;
    if(isset($_POST['NamaModel']))
    {
        $model->attributes=$_POST['Ruangan'];
        $model->status_request_id=0;
        if($model->save())
            $this->redirect(array('view','id'=>$model->id));
    }
    $this->render('create',array(
        'model'=>$model,
    ));
}
public function actionView($id)
{
    return $this->render('view', [
        'model' => $this->findModel($id),
    ]);
}

public function actionUpdates($id)
{
    $model=$this->loadModel($id);
    $model->status_request_id=1;
    if($model->save())
        $this->redirect(array('view','id'=>$model->id));

    $this->render('update',array(
        'model'=>$model,
    ));
}
protected function findModel($id)
{
    if (($model = Ruangan::findOne($id)) !== null) {
        return $model;
    } else {
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}