<?php
namespace backend\controllers;

use Yii;
use backend\models\LpjKompetisi;
use backend\models\LpjKompetisiSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\Notifikasi;

/**
 * LpjKompetisiController implements the CRUD actions for LpjKompetisi model.
 */
class LpjKompetisiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules'=> [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all LpjKompetisi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LpjKompetisiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 5];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

       //Action Download
    public function actionDownload($id) 
    { 
        $download = LpjKompetisi::findOne($id); 
        $path=Yii::getAlias('@frontend') . '/web/LPJ Kompetisi/' . $download->file_lpj;
        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        }
    }

    public function actionApprove($id)
    {
        $model = LpjKompetisi::find()->where(['id_LpjKompetisi' => $id])->one();
        $notif = new Notifikasi();
        $notif->id_mhs = $model->id_mhs;
        $notif->desc = "LPJ Kompetisi Diterima";
        $model->status_request_id=1;
        $searchModel = new LpjKompetisiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if($model->save() && $notif->save(false)){

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
            
        }
    }


    public function actionReject($id)
    {
        $model = LpjKompetisi::find()->where(['id_LpjKompetisi' => $id])->one();
        $notif = new Notifikasi();
        $notif->id_mhs = $model->id_mhs;
        $notif->desc = "LPJ Kompetisi Ditolak";
        $model->status_request_id=2;
        $searchModel = new LpjKompetisiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if($model->save()){
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{

        }
    }
}

    public function actionPdf($id) {
    $model = LpjKompetisi::findOne($id);
        
    

    // This will need to be the path relative to the root of your app.
    $filePath = '/web/LPJ Kompetisi/';
    // Might need to change '@app' for another alias
    $completePath = Yii::getAlias('@frontend'.$filePath.'/'.$model->file_lpj);

    
    return Yii::$app->response->sendFile($completePath, $filePath, ['inline'=>true]);

        
}

    public function actionPdf($id) {
        $model = LpjKompetisi::findOne($id);

    // This will need to be the path relative to the root of your app.
        $filePath = '/web/LPJ Kompetisi/';
    // Might need to change '@app' for another alias
        $completePath = Yii::getAlias('@frontend'.$filePath.'/'.$model->file_lpj);

        
        return Yii::$app->response->sendFile($completePath, $filePath, ['inline'=>true]);


    }


    /**
     * Displays a single LpjKompetisi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LpjKompetisi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model=new LpjKompetisi;
        if(isset($_POST['NamaModel']))
        {
            $model->attributes=$_POST['LpjKompetisi'];
            $model->status_request_id=0;
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }
        $this->render('create',array(
            'model'=>$model,
        ));
    }



    /**
     * Updates an existing LpjKompetisi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdates($id)
    {
        $model=$this->loadModel($id);
        $model->status=1;
        if($model->save())
            $this->redirect(array('view','id'=>$model->id));

        $this->render('update',array(
            'model'=>$model,
        ));
    }
    protected function findModel($id)
    {
        if (($model = LpjKompetisi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
 /**
     * Login action.
     *
     * @return string
     */
 public function actionLogin()
 {
    if (!Yii::$app->user->isGuest) {
        return $this->goHome();
    }

    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
        return $this->goBack();
    } else {
        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }
}


    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}

