-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2020 at 10:57 AM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sikma`
--

-- --------------------------------------------------------

--
-- Table structure for table `beasiswa`
--

CREATE TABLE `beasiswa` (
  `id_beasiswa` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `nama_mhs` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `nim_mhs` varchar(50) NOT NULL,
  `jlh_bersaudara` int(11) NOT NULL,
  `anak_ke` int(11) NOT NULL,
  `tanggungan` int(11) NOT NULL,
  `pekerjaan_ayah` varchar(50) NOT NULL,
  `pekerjaan_ibu` varchar(50) NOT NULL,
  `penghasilan_ayah` int(11) NOT NULL,
  `penghasilan_ibu` int(11) NOT NULL,
  `status_request_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beasiswa`
--

INSERT INTO `beasiswa` (`id_beasiswa`, `id_mhs`, `nama_mhs`, `alamat`, `nim_mhs`, `jlh_bersaudara`, `anak_ke`, `tanggungan`, `pekerjaan_ayah`, `pekerjaan_ibu`, `penghasilan_ayah`, `penghasilan_ibu`, `status_request_id`, `deleted`, `deleted_at`, `deleted_by`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(2, 0, 'Delta', 'Sitoluama', '11317029', 5, 4, 2, 'Petani', 'Petani', 20000000, 2000000, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 0, 'Nopri', 'Silaen', '11317011', 4, 3, 2, '+', 'Petani', 0, 1000000, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 0, 'Jhosua', 'Siantar', '11317044', 4, 3, 2, 'Polisi', 'Guru', 3000000, 3000000, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 0, 'jola', 'dimana', '11317044', 2, 1, 8, 'sadasd', 'sada', 0, 10000, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 0, 'jhosua', 'pematang', '1311317', 1, 1, 7, 'PNS', 'PNS', 120000000, 15000000, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 0, 'jola', 'pematang', '11317044', 2, 1, 3, 'PNS', 'PNS', 120000000, 15000000, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 0, 'jhosua sinambela', 'pematang', '11317044', 2, 1, 3, 'PNS', 'PNS', 120000000, 15000000, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 0, 'jhosua sirait', 'marihat', '1102222', 3, 2, 4, 'TNI', 'Ibu rumahtannga', 10000000, 40000000, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 0, 'jhosua', 'dimana', '11317044', 2, 1, 3, 'PNS', 'Ibu rumahtannga', 10000000, 15000000, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 0, 'jola', 'dimana', '11317044', 2, 1, 3, 'TNI', 'Ibu rumahtannga', 120000000, 40000000, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `beasiswa_company`
--

CREATE TABLE `beasiswa_company` (
  `id_company` int(11) NOT NULL,
  `name_company` varchar(256) NOT NULL,
  `jenis_beasiswa` varchar(255) NOT NULL,
  `tujuan` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beasiswa_company`
--

INSERT INTO `beasiswa_company` (`id_company`, `name_company`, `jenis_beasiswa`, `tujuan`) VALUES
(1, 'Tanoto Foundation', 'Tanoto Foundation', 'Tanoto Foundation'),
(2, 'Ikatan Alumni', 'Ikatan Alumni', 'Ikatan Alumni'),
(3, 'Djarum Super', 'Beasiswa Djarum', 'Beasiswa Djarum'),
(4, 'Lainnya', 'Lainnya', 'Lainnya');

-- --------------------------------------------------------

--
-- Table structure for table `beasiswa_mahasiswa`
--

CREATE TABLE `beasiswa_mahasiswa` (
  `id_beasmaha` int(11) NOT NULL,
  `jns_beasiswa` varchar(256) NOT NULL,
  `informasi_beasiswa` varchar(500) NOT NULL,
  `tnggl_awal` date NOT NULL,
  `tnggl_akhir` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beasiswa_mahasiswa`
--

INSERT INTO `beasiswa_mahasiswa` (`id_beasmaha`, `jns_beasiswa`, `informasi_beasiswa`, `tnggl_awal`, `tnggl_akhir`) VALUES
(3, 'Tanoto Foundation', 'new', '2020-05-07', '2020-05-08'),
(4, 'Ikatan Alumni', 'apaaja', '2020-05-05', '2020-05-06'),
(5, 'beasiswa new', 'kenangan mantan', '2020-05-06', '2020-05-09'),
(6, 'polonia', 'Tidak semata bantuan dana pendidikan, Beasiswa Karya Salemba Empat juga mencakup program pembinaan dari Karya Salemba Empat, seperti tatap muka, leadership program, seminar, workshop, dan coaching. Kegiatan tersebut dilakukan secara online.\r\n\r\nPersyaratan:\r\n\r\n1. Program Beasiswa diutamakan diberikan kepada mahasiswa yang berasal dari keluarga yang kurang mampu\r\n2. Mahasiswa yang bersangkutan belum menikah\r\n3. Mahasiswa yang bersangkutan tidak sedang menerima beasiswa dari fakultas, universitas, ', '2020-05-05', '2020-05-06'),
(7, 'Ikatan Alumni', 'sadsad', '2020-05-05', '2020-05-06'),
(8, 'Quipper', 'Platform belajar online Quipper bekerja sama dengan sejumlah Perguruan Tinggi swasta terkemuka untuk membuka beasiswa S1 di dalam negeri. Beasiswa yang ditawarkan ini di beri nama Quipper Scholarship Award 2020.\r\n\r\nBeasiswa Quipper menawarkan dua jenis beasiswa, yakni beasiswa penuh (fully-funded) dan beasiswa sebagian (partial scholarship). Beasiswa penuh menanggung biaya kuliah penuh setiap semester hingga masa studi selesai. Sedangkan beasiswa sebagian menawarkan diskon atau potongan biaya ku', '2020-05-05', '2020-05-06');

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan`
--

CREATE TABLE `kegiatan` (
  `id_kegiatan` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `jenis_kegiatan` varchar(255) NOT NULL,
  `upload_proposal` varchar(255) NOT NULL,
  `status_request_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kemahasiswaan`
--

CREATE TABLE `kemahasiswaan` (
  `id_kmhs` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nama_kmhs` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kompetisi`
--

CREATE TABLE `kompetisi` (
  `id_kompetisi` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `jenis_kompetisi` varchar(50) NOT NULL,
  `upload_proposal` varchar(50) NOT NULL,
  `status_request_id` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kompetisi`
--

INSERT INTO `kompetisi` (`id_kompetisi`, `id_mhs`, `jenis_kompetisi`, `upload_proposal`, `status_request_id`) VALUES
(6, 1, 'makan bareng dosen', '16.04.2600_bab1.pdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lpj_kegiatan`
--

CREATE TABLE `lpj_kegiatan` (
  `id_LpjKegiatan` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `jenis_kegiatan` varchar(50) NOT NULL,
  `file_lpj` varchar(500) NOT NULL,
  `status_request_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lpj_kegiatan`
--

INSERT INTO `lpj_kegiatan` (`id_LpjKegiatan`, `id_mhs`, `jenis_kegiatan`, `file_lpj`, `status_request_id`) VALUES
(3, 1, 'Car Free Day Del', 'English_Assigment_11317022.pdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lpj_kompetisi`
--

CREATE TABLE `lpj_kompetisi` (
  `id_LpjKompetisi` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `jenis_kompetisi` varchar(50) NOT NULL,
  `file_lpj` varchar(500) NOT NULL,
  `status_request_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lpj_kompetisi`
--

INSERT INTO `lpj_kompetisi` (`id_LpjKompetisi`, `id_mhs`, `jenis_kompetisi`, `file_lpj`, `status_request_id`) VALUES
(3, 1, 'new kompetis IT DEL', '16.04.2600_bab1.pdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id_mhs` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `nama_mhs` varchar(50) NOT NULL,
  `nim_mhs` varchar(50) NOT NULL,
  `prodi` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mhs_resign`
--

CREATE TABLE `mhs_resign` (
  `id_resign` int(11) NOT NULL,
  `id_kmhs` int(11) NOT NULL,
  `nama_mhs` varchar(255) NOT NULL,
  `nim` varchar(255) NOT NULL,
  `prodi` varchar(255) NOT NULL,
  `alasan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mhs_resign`
--

INSERT INTO `mhs_resign` (`id_resign`, `id_kmhs`, `nama_mhs`, `nim`, `prodi`, `alasan`) VALUES
(2, 0, 'jhosua', '11317044', 'D3 Teknik Komputer', 'makan di kmpi');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1580888610),
('m130524_201442_init', 1580888615),
('m190124_110200_add_verification_token_column_to_user_table', 1580888616);

-- --------------------------------------------------------

--
-- Table structure for table `new_user`
--

CREATE TABLE `new_user` (
  `id_mhs` int(10) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `authKey` char(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `new_user`
--

INSERT INTO `new_user` (`id_mhs`, `firstname`, `lastname`, `username`, `password`, `authKey`) VALUES
(1, 'jola', 'sinambela', 'jola', 'jola123', '12345a'),
(2, 'jhosua', 'sinambela', 'jhosua', 'jhosua656', '12345ww'),
(5, 'user', 'user', 'user', 'user123', 'user12');

-- --------------------------------------------------------

--
-- Table structure for table `notifikasi`
--

CREATE TABLE `notifikasi` (
  `notif_id` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `desc` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifikasi`
--

INSERT INTO `notifikasi` (`notif_id`, `id_mhs`, `desc`, `status`) VALUES
(16, 1, 'Lpj Kegiatan Anda Diterima', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prestasi`
--

CREATE TABLE `prestasi` (
  `id_prestasi` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `jenis_prestasi` varchar(255) NOT NULL,
  `tgl_awal_kegiatan` datetime NOT NULL,
  `tgl_akhir_kegiatan` datetime NOT NULL,
  `upload_file` varchar(255) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `status_request_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prestasi`
--

INSERT INTO `prestasi` (`id_prestasi`, `id_mhs`, `jenis_prestasi`, `tgl_awal_kegiatan`, `tgl_akhir_kegiatan`, `upload_file`, `keterangan`, `status_request_id`) VALUES
(1, 1, 'wakwkakaw', '2020-05-06 00:00:00', '2020-05-07 00:00:00', 'Sprint3_ReviewKMHS-yap.pdf', 'new keterangan', 1);

-- --------------------------------------------------------

--
-- Table structure for table `program_studi`
--

CREATE TABLE `program_studi` (
  `prodi_id` int(2) NOT NULL,
  `program_studi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program_studi`
--

INSERT INTO `program_studi` (`prodi_id`, `program_studi`) VALUES
(1, 'D3 Teknologi Informasi'),
(2, 'D3 Teknik Komputer'),
(3, 'D4 Teknologi Rekayasa Perangkat Lunak'),
(4, 'S1 Sistem Informasi'),
(5, 'S1 Teknik Bioproses'),
(6, 'S1 Teknik Informatika'),
(7, 'S1 Manajemen Rekayasa'),
(8, 'S1 Teknik Elektro');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `nama`) VALUES
(1, 'admin'),
(2, 'users');

-- --------------------------------------------------------

--
-- Table structure for table `ruangan`
--

CREATE TABLE `ruangan` (
  `id_ruangan` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL DEFAULT '1',
  `jns_ruangan` varchar(100) NOT NULL,
  `tujuan` varchar(100) NOT NULL,
  `waktu_peminjaman` datetime NOT NULL,
  `waktu_akhir_peminjaman` datetime NOT NULL,
  `status_request_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_request`
--

CREATE TABLE `status_request` (
  `status_request_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_request`
--

INSERT INTO `status_request` (`status_request_id`, `name`) VALUES
(0, 'Menunggu'),
(1, 'Disetujui'),
(2, 'Ditolak');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'jhosua', 'eq1TdscAp-q4TZUMjK3PxFy9-tssifUH', '$2y$13$xxaPz.xsI1yAZ4Txd4/sC.qwoL5s7C4f9P2LBxsYyF59laXKjRUkq', NULL, 'jhosuagaruda389@gmail.com', 9, 1580888730, 1580888730, 'RNHoMirnEWilLmqNtcTwONGhfScwrfPL_1580888730'),
(2, 'jhosuasinambela', 'PhWOP__1PJ6MGWekoLLhjI1vhxn7CbOb', '$2y$13$qlhyCi/1e.wJ2Hw2dNgeZ.l1NIhMALe7sRXOjlQiDiGuR7aYGdocS', NULL, 'januarsimson@gmail.com', 10, 1580889151, 1580889151, NULL),
(3, 'nopri', '4mjkqnQnxMvRLl4agqjNoYeB7S0Imu_X', '$2y$13$VAGTsrVDACzUR.8lW/IOR.tN0N2YKTgVvQXJgesr1KqICgbkCiGOO', NULL, 'nopri@gmail.com', 10, 1584073830, 1584073830, NULL),
(4, 'admin', '7TppFMHBRQP6I9Ejiwh60aOchovPL_NS', '$2y$13$1Vjc6ELGg8zR.tblPeAoJe/yEoWuyKrsw8nJ4UO5kmLKLkhQyfl0y', NULL, 'admin@gmai.com', 10, 1586704890, 1586704890, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beasiswa`
--
ALTER TABLE `beasiswa`
  ADD PRIMARY KEY (`id_beasiswa`);

--
-- Indexes for table `beasiswa_mahasiswa`
--
ALTER TABLE `beasiswa_mahasiswa`
  ADD PRIMARY KEY (`id_beasmaha`);

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`),
  ADD KEY `id_mhs` (`id_mhs`);

--
-- Indexes for table `kemahasiswaan`
--
ALTER TABLE `kemahasiswaan`
  ADD PRIMARY KEY (`id_kmhs`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `kompetisi`
--
ALTER TABLE `kompetisi`
  ADD PRIMARY KEY (`id_kompetisi`),
  ADD KEY `id_mhs` (`id_mhs`);

--
-- Indexes for table `lpj_kegiatan`
--
ALTER TABLE `lpj_kegiatan`
  ADD PRIMARY KEY (`id_LpjKegiatan`);

--
-- Indexes for table `lpj_kompetisi`
--
ALTER TABLE `lpj_kompetisi`
  ADD PRIMARY KEY (`id_LpjKompetisi`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id_mhs`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `mhs_resign`
--
ALTER TABLE `mhs_resign`
  ADD PRIMARY KEY (`id_resign`),
  ADD KEY `id_kmhs` (`id_kmhs`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `new_user`
--
ALTER TABLE `new_user`
  ADD PRIMARY KEY (`id_mhs`);

--
-- Indexes for table `notifikasi`
--
ALTER TABLE `notifikasi`
  ADD PRIMARY KEY (`notif_id`),
  ADD KEY `id_mhs` (`id_mhs`);

--
-- Indexes for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id_prestasi`),
  ADD KEY `id_mhs` (`id_mhs`);

--
-- Indexes for table `program_studi`
--
ALTER TABLE `program_studi`
  ADD PRIMARY KEY (`prodi_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `ruangan`
--
ALTER TABLE `ruangan`
  ADD PRIMARY KEY (`id_ruangan`),
  ADD KEY `id_mhs` (`id_mhs`);

--
-- Indexes for table `status_request`
--
ALTER TABLE `status_request`
  ADD PRIMARY KEY (`status_request_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beasiswa`
--
ALTER TABLE `beasiswa`
  MODIFY `id_beasiswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `beasiswa_mahasiswa`
--
ALTER TABLE `beasiswa_mahasiswa`
  MODIFY `id_beasmaha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kemahasiswaan`
--
ALTER TABLE `kemahasiswaan`
  MODIFY `id_kmhs` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kompetisi`
--
ALTER TABLE `kompetisi`
  MODIFY `id_kompetisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `lpj_kegiatan`
--
ALTER TABLE `lpj_kegiatan`
  MODIFY `id_LpjKegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `lpj_kompetisi`
--
ALTER TABLE `lpj_kompetisi`
  MODIFY `id_LpjKompetisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id_mhs` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mhs_resign`
--
ALTER TABLE `mhs_resign`
  MODIFY `id_resign` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `new_user`
--
ALTER TABLE `new_user`
  MODIFY `id_mhs` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `notifikasi`
--
ALTER TABLE `notifikasi`
  MODIFY `notif_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `prestasi`
--
ALTER TABLE `prestasi`
  MODIFY `id_prestasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `program_studi`
--
ALTER TABLE `program_studi`
  MODIFY `prodi_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ruangan`
--
ALTER TABLE `ruangan`
  MODIFY `id_ruangan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `status_request`
--
ALTER TABLE `status_request`
  MODIFY `status_request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `kompetisi`
--
ALTER TABLE `kompetisi`
  ADD CONSTRAINT `kompetisi_ibfk_1` FOREIGN KEY (`id_mhs`) REFERENCES `new_user` (`id_mhs`);

--
-- Constraints for table `notifikasi`
--
ALTER TABLE `notifikasi`
  ADD CONSTRAINT `notifikasi_ibfk_1` FOREIGN KEY (`id_mhs`) REFERENCES `new_user` (`id_mhs`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
